import json
import os
import random
import string
import time

import http.client
from keras.models import model_from_json
import nltk
import numpy as np
import pandas as pd
import pickle

import config

stemmer = nltk.stem.lancaster.LancasterStemmer()
intents = json.loads(open('intents.json').read())

json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
model = model_from_json(loaded_model_json)
json_file.close()

model.load_weights("model.h5")
print("Loaded model from disk")


with open("words.txt", "rb") as fp:
    words = pickle.load(fp)

with open("classes.txt", "rb") as fp:
    classes = pickle.load(fp)

def clean_up_sentence(sentence):

    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]

    return sentence_words

def bow(sentence, words, show_details=True):
    
    sentence_words = clean_up_sentence(sentence)
    bag = [0]*len(words)  
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s: 
                bag[i] = 1

    return(np.array(bag))

def classify_local(sentence):
    ERROR_THRESHOLD = 0.25
    
    input_data = pd.DataFrame([bow(sentence, words)], dtype=float, index=['input'])
    results = model.predict([input_data])[0]
    results = [[i,r] for i,r in enumerate(results) if r>ERROR_THRESHOLD]
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []

    for r in results:
        return_list.append((classes[r[0]], str(r[1])))
    
    return return_list

def get_football_mathces():
    
    connection = http.client.HTTPConnection('api.football-data.org')
    headers = {'X-Auth-Token': config.auth_token}
    connection.request('GET', '/v2/matches?status=SCHEDULED', None, headers)
    response = json.loads(connection.getresponse().read().decode())

    return response

def getResponse(ints, intents_json):
    
    tag = ints[0][0]
    list_of_intents = intents_json['intents']
    for i in list_of_intents:
        if(i['tag']== tag):
            result = random.choice(i['responses'])
            break
    
    return result

def chatbot_response(text):
    
    ints = classify_local(text)
    if ints[0][0] == 'сьогодні':
        
        response = get_football_mathces()
        mathces_amount = response.get('count')
        smile = '😀' if mathces_amount else '😳'
        
        word_completions = {
            1: 'матч',
            2: 'матчі',
            3: 'матчі',
            4: 'матчі'
        }
        games = []

        word_completion = word_completions.get(mathces_amount) if mathces_amount in word_completions else 'матчів'
        if mathces_amount:
            for match in response.get('matches'):
        
                home_team = match.get('homeTeam', {}).get('name')
                away_team = match.get('awayTeam', {}).get('name')
                games.append(f'{home_team} грає проти {away_team}')
            
            games_reply = '. '.join(games)
        else:
            games_reply = ''
        res = f"Сьогодні можна переглянути {mathces_amount} {word_completion} {smile}. {games_reply}"
    else:
        res = getResponse(ints, intents)
    
    return res


def chat():
    print("Починай спілкуватися із ботом. Для завершення напиши 'Бувай!'.")
    while True:
        phrase = input("You: ")
        if phrase.lower() == "quit":
            break

        phrase = phrase.lower()
        phrase = phrase.translate(str.maketrans('', '', string.punctuation))
        response = chatbot_response(phrase)
        print('Bot: ' + response)
        
        time.sleep(1)
        if response in intents.get('intents')[1].get('responses'):
            break


chat()