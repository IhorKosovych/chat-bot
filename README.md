# Чат-бот

"Чат-бот" is an application implemented using neural networks and will be able to understand what the user is talking about and give an appropriate response for football matches in ukrainian like below sample conversation.

![Farmers Market Finder Demo](media/bot.gif)

## Running

To be able to run, you can simply activate virtualenv and run 

```bash
python test.py
```

## Description
This neural network is trained using Keras module, classify your message to the group it is trained on it. If you are trying to find out which football match you can watch today then API request is making to get the result from [I'm an inline-style link] api.football-data.org 